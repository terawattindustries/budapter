githubiverse-template
============

This is a 3D Printer part designed in OpenSCAD.  This part is an adapter for using a Budschnozzle with a j-head compatible extruder.  The Budapter reduces filament kinking by filling the cavity where the j-head would normally attach.

Instructions
------------

* Download the STL (or download the SCAD, modify, and export).
* Print the part, or obtain a printed one.
* Place the Budapter into the space where the j-head normally attaches.  If your extruder is already assembled, then you must disassemble to attach the extruder.  On Kuehling's x-carriage this is performed by removing the 2x M4x20mm socket screws from the bottom of the carriage.
* Re-asssemble the extruder.
* Replicate your printer.

License
-------
SCAD source + distro:  GPLv3
ANT Build System:  Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.  For a commercial license please contact the author.